---

# this task requires lot of CPU cycles, and takes forever on RPi, even on RPi 4 with Raspbian 10
# note the matching if-statement in the ssl-params.conf.j2 file
# https://docs.ansible.com/ansible/latest/collections/community/crypto/openssl_dhparam_module.html
# WARNING: this task is expected to take ~15 min on a 1 vCPU VPS, so the
# ssh connection parameters in ansible.cfg matter!
# ssh_args = -o ServerAliveInterval=30 -o ServerAliveCountMax=3 # appears to work reliably, but
# ssh_args = -o ControlMaster=auto -o ControlPersist=30m # has proven to fail
- name: Generate custom Diffie-Hellman parameters
  community.crypto.openssl_dhparam:
    path: "{{ dhgroup_key }}"
    state: present
    size: 4096
  register: dhparam_custom
  when:
    - ansible_lsb.id != "Raspbian"
    - not (host_restrictions | bool) or group_names is not search("lxd_containers")
    - not (host_restrictions | bool) or group_names is not search("onlyoffice_server")

# https://stackoverflow.com/questions/46272835/how-to-set-up-apache2-and-php-fpm-via-unix-socket
# https://cwiki.apache.org/confluence/display/HTTPD/PHP-FPM
- name: Configure Apache php-fpm.conf
  ansible.builtin.template:
    src: php-fpm.conf.j2
    dest: "{{ apache_conf_path }}/conf-available/php{{ php_version }}-fpm.conf"

- name: Enable Apache php-fpm.conf
  ansible.builtin.file:
    src: "{{ apache_conf_path }}/conf-available/php{{ php_version }}-fpm.conf"
    dest: "{{ apache_conf_path }}/conf-enabled/php{{ php_version }}-fpm.conf"
    state: link
  notify: restart apache


- name: Configure SSL
  when: apache_enable_ssl | bool
  block:

    # https://docs.ansible.com/ansible/latest/collections/community/general/apache2_module_module.html
    - name: Enable the Apache2 module SSL
      community.general.apache2_module:
        state: present
        name: ssl

    - name: Configure Apache ssl-params.conf (Bionic/Focal)
      ansible.builtin.template:
        src: ssl-{{ ansible_distribution_major_version | int }}.conf.j2
        dest: "{{ apache_conf_path }}/conf-available/ssl-params.conf"
      when: ansible_distribution_major_version | int <= 20

    - name: Configure Apache ssl-params.conf (Jammy)
      ansible.builtin.template:
        src: ssl-{{ ansible_distribution_major_version | int }}.conf.j2
        dest: "{{ apache_conf_path }}/conf-available/ssl-params.conf"
      when: ansible_distribution_major_version | int >= 22

    - name: Enable Apache ssl-params.conf
      ansible.builtin.file:
        src: "{{ apache_conf_path }}/conf-available/ssl-params.conf"
        dest: "{{ apache_conf_path }}/conf-enabled/ssl-params.conf"
        state: link
      notify: restart apache
  # END OF BLOCK


- name: Configure Apache ports.conf
  ansible.builtin.lineinfile:
    dest: "{{ apache_server_root }}/ports.conf"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    state: present
  loop: "{{ apache_ports_configuration_items }}"
  notify: restart apache

- name: Enable Apache mods
  ansible.builtin.file:
    src: "{{ apache_server_root }}/mods-available/{{ item }}"
    dest: "{{ apache_server_root }}/mods-enabled/{{ item }}"
    state: link
  loop: "{{ apache_mods_enabled }}"
  notify: restart apache

- name: Disable Apache mods
  ansible.builtin.file:
    path: "{{ apache_server_root }}/mods-enabled/{{ item }}"
    state: absent
  loop: "{{ apache_mods_disabled }}"
  notify: restart apache

- name: Remove default vhost in sites-enabled
  ansible.builtin.file:
    path: "{{ apache_conf_path }}/sites-enabled/{{ apache_default_vhost_filename }}"
    state: absent
  notify: restart apache
  when: apache_remove_default_vhost

- name: Create ServerName configuration file for Apache
  ansible.builtin.copy:
    content: "ServerName {{ APACHE_FQDN }}\n"
    dest: "{{ apache_conf_path }}/conf-available/fqdn.conf"

- name: Set ServerName for Apache
  ansible.builtin.command: a2enconf fqdn
  args:
    creates: "{{ apache_conf_path }}/conf-enabled/fqdn.conf"
  notify: restart apache
